package pl.test.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.hibernate.DAO.PersonDAO;
import pl.hibernate.entites.Person;

import java.io.IOException;


public class NewAccount {
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private DatePicker birth_date;
    @FXML
    private TextField address;
    @FXML
    private TextField pesel;
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private Label error;

    public void createNewAccount(ActionEvent event) throws IOException {
        try {
            PersonDAO personDAO = new PersonDAO();
            personDAO.createAccount(userName.getText(), password.getText(), birth_date.getValue(), address.getText(), pesel.getText(), firstName.getText(), lastName.getText());
            Parent tableViewParen = FXMLLoader.load(getClass().getResource("/mainController.fxml"));
            Scene tableViewScene = new Scene(tableViewParen);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);
            window.setTitle("Create new Account");
            window.show();
            userName.clear();
            password.clear();
            address.clear();
            pesel.clear();
            firstName.clear();
            lastName.clear();
        } catch (RuntimeException e) {
            error.setText("User name exists");


        }

    }
}
