package pl.test.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import pl.hibernate.DAO.WarehouseDAO;
import pl.hibernate.entites.Person;
import pl.hibernate.entites.Warehouse;
import pl.test.controllers.MainController;
import pl.hibernate.DAO.RoomDAO;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class UserWindow implements Initializable {
    MainController mainController = new MainController();

    @FXML
    private TableView roomList;
    @FXML
    private Label loggedAs;
    @FXML
    private ComboBox warehouse;


    public void logOut(ActionEvent event) throws IOException {
        Parent tableViewParen = FXMLLoader.load(getClass().getResource("/mainController.fxml"));
        Scene tableViewScene = new Scene(tableViewParen);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }

    public void displayRooms(Event event) {
        RoomDAO roomDAO = new RoomDAO();
        roomList.getItems().addAll(roomDAO.printAllRooms());
        roomList.refresh();
    }

    public void loggedAs(String username) {
        loggedAs.setText(username);

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loggedAs.setText("You are logged in as: " + MainController.username);
        WarehouseDAO warehouseDAO = new WarehouseDAO();
        warehouse.getItems().addAll(warehouseDAO.listOfWarehouse());

    }
//    @Override
//    public void initialize(URL url, ResourceBundle resourceBundle) {
//        loggedAs.setText(String.valueOf(MainController.userName));
//
//    }
}
// TODO: 05/12/2019 user window must contain list of rooms in ware house which you can rent and when is free you can check it and save as you rent these room
// TODO: 05/12/2019 you can print all your rooms which you rent and you get option to extend time to rent when your rent time finish before 3 days