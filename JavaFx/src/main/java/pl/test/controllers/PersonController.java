package pl.test.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import pl.hibernate.DAO.PersonDAO;

import java.io.IOException;

public class PersonController {
    @FXML
    private TextField firstName;
    @FXML
    private TextField surname;
    @FXML
    private TextField PESEL;
    @FXML
    private TextField address;
    @FXML
    private DatePicker birth_date;
    @FXML
    private ListView table;

    public void addPerson(ActionEvent event) throws IOException {
        PersonDAO personDAO = new PersonDAO();
        personDAO.addPerson(firstName.getText(), surname.getText(), address.getText(), PESEL.getText(), birth_date.getValue());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Notification");
        alert.setHeaderText(null);
        alert.setContentText("Created person: " + "\"" + firstName.getText() + "\"");
        alert.showAndWait();
        alert.close();
        firstName.clear();
        surname.clear();
        address.clear();
        birth_date.getEditor().clear();
        PESEL.clear();


    }


    public void printAllPersons() {
        PersonDAO personDAO = new PersonDAO();
        table.getItems().addAll(personDAO.printNamePersons());
        table.refresh();
    }


    public void goToNewScene(ActionEvent event) throws IOException {
        Parent tableViewParen = FXMLLoader.load(getClass().getResource("/rooms.fxml"));
        Scene tableViewScene = new Scene(tableViewParen);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.setTitle("Rooms");
        window.show();
    }
}
