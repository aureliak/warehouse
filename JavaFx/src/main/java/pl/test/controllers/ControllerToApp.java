package pl.test.controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.hibernate.DAO.RoomDAO;
import pl.hibernate.DAO.WarehouseDAO;
import pl.hibernate.entites.Room;
import pl.hibernate.entites.Warehouse;

import java.net.URL;
import java.util.ResourceBundle;


public class ControllerToApp extends Application {


    @Override
    public void start(Stage stage) throws Exception {


        RoomDAO roomDAO = new RoomDAO();
        roomDAO.addRoom();
        Parent root = FXMLLoader.load(getClass().getResource("/mainController.fxml"));
        stage.setTitle("Warehouse");
        stage.setScene(new Scene(root, 350, 500));
        stage.show();

    }


    public static void main(String[] args) {
        launch(args);


    }

}
