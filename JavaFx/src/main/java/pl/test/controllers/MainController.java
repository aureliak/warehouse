package pl.test.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.hibernate.DAO.PersonDAO;
import pl.hibernate.DAO.WarehouseDAO;
import pl.hibernate.entites.Person;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    public static String username;

    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private Label status;

    public  Person person = new Person();

    public Person logIn(ActionEvent event) throws IOException {
        try {
            PersonDAO personDAO = new PersonDAO();
            username = userName.getText();
            person.setUserName(username);
            person.setPassword(password.getText());
            boolean user = personDAO.isLoggInCorrect(person);
            if (user == true) {
                status.setText("Bad password or userName");
            } else {
                status.setText("Hello");
                Parent tableViewParen = FXMLLoader.load(getClass().getResource("/userWindow.fxml"));
                Scene tableViewScene = new Scene(tableViewParen);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(tableViewScene);

                window.setTitle("User window");
                window.show();
                return person = new Person();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void goCreateNewAccount(ActionEvent event) throws IOException {
        Parent tableViewParen = FXMLLoader.load(getClass().getResource("/createNewAccount.fxml"));
        Scene tableViewScene = new Scene(tableViewParen);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.setTitle("Create new Account");
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        WarehouseDAO warehouseDAO = new WarehouseDAO();
        if (warehouseDAO.warehouseIsCreated()) { // zwroci true jezeli nie ma nic
            warehouseDAO.createWarehouse();
        }
    }
}
