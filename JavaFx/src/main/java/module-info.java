module JavaFx {

    exports pl.test.controllers;
    requires javafx.fxml;
    requires javafx.graphics;
    requires Hibernate;
    requires javafx.controls;


    opens pl.test.controllers;

}