package pl.hibernate.DAO;

import pl.hibernate.ConnectionFactory;
import pl.hibernate.entites.Person;
import pl.hibernate.entites.Warehouse;

import javax.persistence.Query;
import java.util.List;

import static pl.hibernate.ConnectionFactory.entityManager;

public class WarehouseDAO {

    ConnectionFactory connectionFactory = new ConnectionFactory();

    public void createWarehouse() {

            Warehouse warehouse1 = new Warehouse();
            warehouse1.setName("Warehouse 1");
            warehouse1.setAddres("Poland");
            Warehouse warehouse2 = new Warehouse();
            warehouse2.setName("Warehouse 2");
            warehouse2.setAddres("Spain");
            entityManager.getTransaction().begin();
            entityManager.persist(warehouse1);
            entityManager.persist(warehouse2);
            entityManager.getTransaction().commit();
        }


//        isCreated = true;


    public boolean warehouseIsCreated() {
//        entityManager.getTransaction().begin();
         Query query1 = entityManager.createQuery("select w.name from Warehouse w ");
        List resultList = query1.getResultList();
        boolean empty = resultList.isEmpty();

        return empty;
    }
    public List<Warehouse> listOfWarehouse(){
        Query query1 = entityManager.createQuery("select w.name from Warehouse w ");
        List<Warehouse> resultList = query1.getResultList();
        return resultList;
    }


}



