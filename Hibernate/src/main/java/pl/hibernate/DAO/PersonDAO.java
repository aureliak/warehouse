package pl.hibernate.DAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.hibernate.ConnectionFactory;
import pl.hibernate.entites.Person;

import javax.persistence.Query;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import static pl.hibernate.ConnectionFactory.entityManager;


public class PersonDAO {
    ConnectionFactory connectionFactory = new ConnectionFactory();
    Logger logger = LoggerFactory.getLogger(PersonDAO.class);
    Scanner scanner = new Scanner(System.in);



    public void addPerson(String firstName, String lastName, String address, String pesel, LocalDate birtDate) {
        entityManager.getTransaction().begin();
        Person person = new Person();
        person.setFirst_name(firstName);
        person.setLast_name(lastName);
        person.setAddres(address);
        person.setPesel(pesel);
        person.setBirth_date(birtDate);
        entityManager.persist(person);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void printAllPerson() {
        Query query = entityManager.createQuery("select p from Person p");
        query.getResultList().forEach(person -> logger.info(person.toString()));
        entityManager.close();

    }

    public List<Person> listOfPersons() {
        Query query = entityManager.createQuery("select p.person_id from Person p");
        final List resultList = query.getResultList();
        resultList.forEach(person -> logger.info(person.toString()));

        return resultList;


    }


    public void updateById(int id, String firtName) {
        entityManager.getTransaction().begin();
        Person person = entityManager.find(Person.class, id);
        if (person.setFirst_name(null).equals(null)) {
            person.setFirst_name(person.getFirst_name());
        } else {
            person.setFirst_name(firtName);
        }

        if (person.setLast_name(null).equals(person))

            entityManager.getTransaction().commit();
        entityManager.close();
    }


    public void deleteById(int id) {
        entityManager.getTransaction().begin();
        Person personFind = entityManager.find(Person.class, id);
        entityManager.remove(personFind);
        entityManager.getTransaction().commit();
        entityManager.close();
    }


    public List<String> printNamePersons() {
        Query query = entityManager.createQuery("select p.first_name from Person p");
        return query.getResultList();

    }


    public void createAccount(String userName, String password, LocalDate date, String address, String PESEL, String firstName, String lastName) {
        Person person = new Person();
        person.setUserName(userName);
        person.setPassword(password);
        person.setBirth_date(date);
        person.setAddres(address);
        person.setPesel(PESEL);
        person.setFirst_name(firstName);
        person.setLast_name(lastName);
        entityManager.getTransaction().begin();
        entityManager.persist(person);
        entityManager.getTransaction().commit();


    }


    public boolean isLoggInCorrect(Person person) {
        final Query query1 = entityManager.createQuery("select p.userName from Person p where p.userName = ?1 and p.password = ?2");
        query1.setParameter(1, person.getUserName());
        query1.setParameter(2, person.getPassword());
        List resultList = query1.getResultList();
        boolean empty = resultList.isEmpty();

        return empty;
    }

    public String getUserNameFromLoggedUser(Person person) {
        return person.getUserName();


    }



}
