package pl.hibernate.DAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.hibernate.ConnectionFactory;
import pl.hibernate.entites.Categories;
import pl.hibernate.entites.Item;
import pl.hibernate.entites.Room;

import javax.persistence.Query;
import java.util.List;
import java.util.Scanner;

import static pl.hibernate.ConnectionFactory.entityManager;

public class ItemDAO {
    Scanner scanner = new Scanner(System.in);
    Item item = new Item();
    Categories categories;

    Logger logger = LoggerFactory.getLogger(ItemDAO.class);
    ConnectionFactory connectionFactory = new ConnectionFactory();


    public void addItem() {
        logger.info("Podaj nazwę rzeczy");
        final String name = scanner.nextLine();
        item.setName(name);
        logger.info("Podaj wymaiary przedmiotu");
        item.setVolume(countVolume());
        logger.info("Podaj id pomieszczenia znajdującego się przedmiotu");
        final int idRoom = scanner.nextInt();
        final Room room = entityManager.find(Room.class, idRoom);
        item.setRoom(room);
        entityManager.getTransaction().begin();
        entityManager.persist(item);
        entityManager.getTransaction().commit();

    }

    private double countVolume() {
        logger.info("Podaj wysokośc");
        double height = scanner.nextDouble();
        logger.info("Podaj długośc");
        double length = scanner.nextDouble();
        logger.info("Podaj szerokość");
        double width = scanner.nextDouble();
        double volume = height * width * length;
        return volume;
    }

    public void printAllItemsByIdRoom() {
        Query query = entityManager.createQuery("select i from Item i where room_id = ?1");
        logger.info("Podaj id pomieszczenia ");
        int roomId = scanner.nextInt();
        query.setParameter(1, roomId);
        List resultList = query.getResultList();
        resultList.forEach(item -> logger.info(item.toString()));
//        entityManager.getTransaction().begin();
//        entityManager.getTransaction().commit();


    }

    public void printAllItems() {
        Query query = entityManager.createQuery("SELECT i FROM Item i");
        query.getResultList().forEach(item -> System.out.println(item.toString()));
    }


    public void updateItem() {
        Scanner scanner = new Scanner(System.in);
        item = entityManager.find(Item.class, scanner.nextInt());
        System.out.println("Proszę podać nazwę przedmiotu: ");
        item.setName(scanner.nextLine());
        System.out.println("Proszę wprowadzić opis przedmiotu: ");
        item.setDescription(scanner.nextLine());
        System.out.println("Proszę podać wysokość przedmiotu: ");
        double height = scanner.nextDouble();
        System.out.println("Proszę podać szerokość przedmiotu: ");
        double width = scanner.nextDouble();
        System.out.println("Proszę podać długość przedmiotu: ");
        double length = scanner.nextDouble();
        item.setVolume(countVolume());
        entityManager.getTransaction().begin();
        entityManager.persist(item);
        entityManager.getTransaction().commit();
    }

    public void deleteItemById() {
        logger.info("Podaj id przedmiotu");
        final int item_id = scanner.nextInt();
        Item item = entityManager.find(Item.class, item_id);
        entityManager.getTransaction().begin();
        entityManager.remove(item);
        entityManager.getTransaction().commit();
    }


}

