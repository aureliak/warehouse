package pl.hibernate.DAO;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.hibernate.ConnectionFactory;
import pl.hibernate.entites.Person;
import pl.hibernate.entites.Room;

import javax.persistence.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static pl.hibernate.ConnectionFactory.entityManager;


public class RoomDAO {
    Scanner scanner = new Scanner(System.in);

    ConnectionFactory connectionFactory = new ConnectionFactory();
    Logger logger = LoggerFactory.getLogger(RoomDAO.class);


    public void addRoom() {

        entityManager.getTransaction().begin();
        Room room = new Room();
        room.setRent_start_date(LocalDate.now());
        room.setEndDateRent(LocalDate.of(2021, 10, 16));

        entityManager.persist(room);
        entityManager.getTransaction().commit();


    }


    public List<Room> printAllRooms() {
        logger.info("print all rooms ");
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("select r from Room r");
//        for (Room room: query.getResultList()) {
//            System.out.println(room.getDay_left_for_rent());
//        }
        return query.getResultList();


//        entityManager.find(Room.class, 1);

    }


    public void updateRoomById() {
        System.out.println("Podaj id pomieszczenia");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        final Room room = entityManager.find(Room.class, id);
        int choose = scanner.nextInt();
        logger.info("Co chcesz zmienić?");
        logger.info("1. Zmiana właściciela");//czyli szukam tą osobę i ustawiam ją jako nowy właściciel
        logger.info("3. Rozpoczęcie daty wynajmu ");
        int person_id = scanner.nextInt();
        switch (choose) {

            case 1: {
                Person person1 = entityManager.find(Person.class, person_id);
                if (person1 == null)
                    return;
//                    room.setPerson(Person.addPerson);
                else
                    room.setPerson(person1);
            }
            case 2: {
                entityManager.getTransaction().begin();
                System.out.println("Podaj ilość dni najmu");
                int additionalDays = scanner.nextInt();
                room.setEndDateRent(LocalDate.now().plusDays(additionalDays));
                entityManager.persist(room);
                entityManager.getTransaction().commit();
            }
        }
    }


    public void updateAllQery() {
        int sizeList = entityManager.createQuery("SELECT room_id FROM Room").getResultList().size();
        List<Integer> listOfId = entityManager.createQuery("SELECT room_id FROM Room").getResultList();
        System.out.println("ID pobrane z bazy danych: " + listOfId);
        for (int i = 0; i < sizeList; i++) {
            final Integer id = listOfId.get(i);
//            TypedQuery<Room> query = entityManager.createQuery("update Room set day_left_for_rent = DATEDIFF(endDateRent, now()) where room_id = ?1", Room.class);
            entityManager.getTransaction().begin();
            Query query = entityManager.createQuery("UPDATE Room SET day_left_for_rent = DATEDIFF(endDateRent,now()) WHERE room_id = ?1");
            query.setParameter(1, id).executeUpdate();


//        }
            entityManager.getTransaction().commit();

        }


    }


    public void deleteRoomById() {

        System.out.println("O jakim id chcesz usunąć pokój");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
//        Room room = entityManager.find(Room.class, id);
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM Room  WHERE room_id =  ?1");
        query.setParameter(1, id).executeUpdate();
//         entityManager.persist(query);
        entityManager.getTransaction().commit();

    }

    public void addPerosnToRoom() {
        logger.info("Do kogo chcesz dodać pomieszczenie?");
        int personId = scanner.nextInt();
        logger.info("Jakie pomeiszczenie chcesz dodac do osoby o id " + personId);
        int roomId = scanner.nextInt();
        List<Room> rooms = new ArrayList<>();

        //szukanie osoby po username
        entityManager.getTransaction().begin();
        Person person = entityManager.find(Person.class, personId);
        Room room = entityManager.find(Room.class, roomId);
        room.setPerson(person);
        entityManager.persist(room);
        entityManager.persist(person);
        entityManager.getTransaction().commit();
        entityManager.refresh(person);


    }


}


