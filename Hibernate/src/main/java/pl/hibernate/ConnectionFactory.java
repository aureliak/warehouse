package pl.hibernate;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static javax.persistence.Persistence.createEntityManagerFactory;


public class ConnectionFactory {
    public static EntityManagerFactory entityManagerFactory;
    public static EntityManager entityManager;


    public ConnectionFactory() {
        entityManagerFactory = createEntityManagerFactory("pu");
        entityManager = entityManagerFactory.createEntityManager();
    }
}
