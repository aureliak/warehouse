package pl.hibernate.entites;

import javax.persistence.*;
import java.util.List;

@Entity
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int warehouse_id;
    @OneToMany(mappedBy = "warehouse")
    private List<Room> rooms;
    private String name;
    private String addres;

    public Warehouse(List<Room> rooms, String name, String addres) {
//        this.rooms = rooms;
        this.name = name;
        this.addres = addres;
    }

    public Warehouse() {
    }

//    public List<Room> getRooms() {
//        return rooms;
//    }

//    public Warehouse setRooms(List<Room> rooms) {
//        this.rooms = rooms;
//        return this;
//    }

    public String getName() {
        return name;
    }

    public Warehouse setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddres() {
        return addres;
    }

    public Warehouse setAddres(String addres) {
        this.addres = addres;
        return this;
    }
}
