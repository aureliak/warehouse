package pl.hibernate.entites;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int person_id;
    @Column(unique = true)
    private String userName;
    private String password;
    private String first_name;
    private String last_name;
    private String pesel;
    private String addres;
    private LocalDate birth_date;
    @OneToMany(mappedBy = "person")
    private List<Room> rented_rooms;


    public Person() {
    }

    public Person(String first_name, String last_name, String pesel, String addres, LocalDate birth_date, List<Room> rented_rooms) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.pesel = pesel;
        this.addres = addres;
        this.birth_date = birth_date;
        this.rented_rooms = rented_rooms;
    }

    public String getUserName() {
        return userName;
    }

    public Person setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Person setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getPerson_id() {
        return person_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public Person setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public String getLast_name() {
        return last_name;
    }

    public Person setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    public String getPesel() {
        return pesel;
    }

    public Person setPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public String getAddres() {
        return addres;
    }

    public Person setAddres(String addres) {
        this.addres = addres;
        return this;
    }

    public LocalDate getBirth_date() {
        return birth_date;
    }

    public Person setBirth_date(LocalDate birth_date) {
        this.birth_date = birth_date;
        return this;
    }

    public List<Room> getRented_rooms() {
        return rented_rooms;
    }

    public Person setRented_rooms(List<Room> rented_rooms) {
        this.rented_rooms = rented_rooms;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "person_id=" + person_id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", pesel='" + pesel + '\'' +
                ", addres='" + addres + '\'' +
                ", birth_date=" + birth_date +
                ", rented_rooms=" + rented_rooms +
                '}';
    }

}

