package pl.hibernate.entites;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int room_id;
    @ManyToOne
    @JoinColumn(name = "werehouse_id")
    private Warehouse warehouse;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
    private double usable_area;
    private LocalDate rent_start_date;
    private int day_left_for_rent;
    @OneToMany(mappedBy = "room")
    private List<Item> items;
    private LocalDate endDateRent;
    private boolean isEmpty = true;


    public int getRoom_id() {
        return room_id;
    }

    public LocalDate getRent_start_date() {
        return rent_start_date;
    }

    public LocalDate getEndDateRent() {
        return endDateRent;
    }

    public Room setRent_start_date(LocalDate rent_start_date) {
        this.rent_start_date = rent_start_date;
        return this;
    }

    public Room setEndDateRent(LocalDate endDateRent) {
        this.endDateRent = endDateRent;
        return this;
    }


    public Room() {
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }


    public Person getPerson() {
        return person;
    }

    public Room setPerson(Person person) {
        this.person = person;
        return this;
    }

    public double getUsable_area() {
        return usable_area;
    }

    public Room setUsable_area(double usable_area) {
        this.usable_area = usable_area;
        return this;
    }


    public int getDay_left_for_rent() {
        return day_left_for_rent;
    }

    public Room setDay_left_for_rent(int day_left_for_rent) {
        this.day_left_for_rent = day_left_for_rent;
        return this;
    }

    public List<Item> getItems() {
        return items;
    }

    public Room setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return "Room{" +
                "room_id=" + room_id +
                ", warehouse=" + warehouse +
                ", usable_area=" + usable_area +
                ", rent_start_date=" + rent_start_date +
                ", day_left_for_rent=" + day_left_for_rent +
                ", items=" + items +
                ", endDateRent=" + endDateRent +
                '}';
    }
}
