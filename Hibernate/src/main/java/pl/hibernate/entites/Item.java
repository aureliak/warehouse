package pl.hibernate.entites;

import javax.persistence.*;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int item_id;
    private String name;


    private double volume;
    private String description;
    @Enumerated(EnumType.STRING)
    private Categories category;
    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    public Item(String name, double volume, String description, Categories category) {
        this.name = name;
        this.volume = volume;
        this.description = description;
        this.category = category;
    }

    public Item() {
    }

    public int getItem_id() {
        return item_id;
    }

    public String getName() {
        return name;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public double getVolume() {
        return volume;
    }

    public Item setVolume(double volume) {
        this.volume = volume;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Item setDescription(String description) {
        this.description = description;
        return this;
    }

    public Room getRoom() {
        return room;
    }

    public Item setRoom(Room room) {
        this.room = room;
        return this;
    }

    public Categories getCategory() {
        return category;
    }

    public Item setCategory(Categories category) {
        this.category = category;
        return this;
    }

    @Override
    public String toString() {
        try {
            return "Item{" +
                    "name='" + name +
                    ", volume=" + volume +
                    ", description='" + description +
                    ", categorie=" + category +
                    ", room=" + room.getRoom_id() +
                    '}';
        } catch (NullPointerException e) {
            return "Item{" +
                    "name='" + name +
                    ", volume=" + volume +
                    ", description='" + description +
                    ", categorie=" + category +
                    ", room = 0" +
                    '}';
        }

    }
}



