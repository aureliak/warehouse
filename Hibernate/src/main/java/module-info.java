module Hibernate {
    exports pl.hibernate;
    exports pl.hibernate.DAO;
    exports pl.hibernate.entites;
    requires java.persistence;
    requires com.fasterxml.classmate;
    requires net.bytebuddy;
    requires java.sql;
    requires java.xml.bind;
    requires org.hibernate.commons.annotations;
    requires org.hibernate.orm.core;
    requires org.jboss.logging;
    requires slf4j.api;

    opens pl.hibernate;
    opens pl.hibernate.entites;
    opens pl.hibernate.DAO;
}